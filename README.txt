Address/Contact Module  : Address
Original Author         : David Gildeh
Settings                : > administer > store > settings > eticket

********************************************************************
DESCRIPTION:

Adds eticket product type to eCommerce, which allows details of each 
ticket holder to be entered upon checkout.

********************************************************************

See MAINTAINERS.txt for more maintenance info.
See README.txt (in E-Commerce root) for other info.
